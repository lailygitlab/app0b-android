package nuraini.laily.appx0ab

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity(), View.OnClickListener {

    val MHS : Int = 100
    val PRODI : Int = 101


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnMhs ->{
                var intent = Intent(this,MainActivity::class.java)
                startActivityForResult(intent,MHS)
            }
            R.id.btnProdi ->{
                var intent = Intent(this,MainActivity::class.java)
                startActivityForResult(intent,PRODI)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        btnMhs.setOnClickListener(this)
        btnProdi.setOnClickListener(this)


    }


}
