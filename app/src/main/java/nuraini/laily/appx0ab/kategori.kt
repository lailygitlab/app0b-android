package nuraini.laily.appx0ab

import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.kategorilayout.*
import org.json.JSONArray
import org.json.JSONObject
import wahidah.hildha.app0ab.R
import java.util.*
import kotlin.collections.HashMap

class kategori : AppCompatActivity() , View.OnClickListener {

    lateinit var adapProdi: AdapProdi
    var daftarprodi = mutableListOf<HashMap<String,String>>()
    var uri1 = "http://192.168.43.126/Kampus/show_prodi.php"
    var uri2 = "http://192.168.43.126/Kampus/query_prodi.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.kategorilayout)

        adapProdi =  AdapProdi(daftarprodi,this)
        lsprodi.layoutManager = LinearLayoutManager(this)
        lsprodi.adapter = adapProdi

        btin.setOnClickListener(this)
        bthap.setOnClickListener(this)
        btup.setOnClickListener(this)

    }

    override fun onStart() {
        super.onStart()
        Showprod()
    }


    fun Showprod(){
        val request = StringRequest(Request.Method.POST,uri1, Response.Listener { response ->
            daftarprodi.clear()
            val jsonArray = JSONArray(response)
            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var prodi = HashMap<String,String>()
                prodi.put("nama_prodi",jsonObject.getString("nama_prodi"))
                prodi.put("id_prodi",jsonObject.getInt("id_prodi").toString())

                //mhs.put("tanggal",jsonObject.getString("tangal_lahir"))
                //mhs.put("jenis_kelamin",jsonObject.getString("jenis_kelamin"))
                // mhs.put("alamat",jsonObject.getString("alamat"))
                daftarprodi.add(prodi)
            }
            adapProdi.notifyDataSetChanged()
        }, Response.ErrorListener { error ->
            Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
        })
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }



    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri2,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                // var nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(
                //     Date()
                // )+".jpg"
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama_prodi",namaprod.text.toString())

                    }
                    "update"->{
                        hm.put("mode","update")
                        // hm.put("nim",txNm.text.toString())
                        //hm.put("nama",txNamaMhs.text.toString())
                        //hm.put("image",imStr)
                        //hm.put("file",nmFile)
                        //hm.put("nama_prodi",pilihprodi)
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("nama_prodi",namaprod.text.toString())

                        //hm.put("nim",txNm.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btin->{
                query("insert")
                Showprod()

            }
            R.id.bthap->{
                query("delete")
                Showprod()

            }
            R.id.btUpdate->{
                query("update")
                Showprod()
            }
        }
    }



}
