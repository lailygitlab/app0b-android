package nuraini.laily.appx0ab

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.kategorilayout.*
import nuraini.laily.appx0b.app0b-android.kategori

class AdapProdi(val dataprod : List<HashMap<String,String>>,val kat : kategori) : RecyclerView.Adapter<AdapProdi.HolderProdi> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapProdi.HolderProdi {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_prodi,parent,false)
        return HolderProdi(v)
    }

    override fun getItemCount(): Int {
        return dataprod.size
    }

    override fun onBindViewHolder(holder: AdapProdi.HolderProdi, position: Int) {
        val data =dataprod.get(position)
        holder.txprod.setText(data.get("nama_prodi"))
        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            //val pos = mainActi.daftarprodi.indexOf(data.get("nama_prodi"))
            //mainActivity.SpProdi.setSelection(pos)
            kat.namaprod.setText(data.get("nama_prodi"))

        })


    }




    class HolderProdi(v : View) :  RecyclerView.ViewHolder(v){

    val txprod = v.findViewById<TextView>(R.id.prod)
    val layouts = v.findViewById<ConstraintLayout>(R.id.prodia)
    }

}